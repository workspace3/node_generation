# -*- coding: utf-8 -*-
"""
"""
import os
import sys
from bert4keras.backend import keras, K
from bert4keras.optimizers import Adam
from bert4keras.models import build_transformer_model
import numpy as np
from bert4keras.snippets import DataGenerator
from keras.models import Model
from keras.layers import Layer
from nltk.translate.bleu_score import corpus_bleu

os.environ["CUDA_VISIBLE_DEVICES"] = "0"

set_type = "hasA"


# bert配置
project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../../"
))
config_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/lm_albert_config.json"))
dict_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/{}_nodes.txt".format(set_type)
    ))
model_path = os.path.normpath(os.path.join(
    project_dir, "models/best_model_{}.weights".format(set_type)
))

sys.path.insert(0, project_dir)
from src.data_process.load_data import load_hasA, load_isA

HEAD = "[CLS]"
TAIL = "[SEP]"
PAD = "[PAD]"
UNKNOWN = "[UNK]"


class Tokenizer:
    def __init__(self, dict_path):
        with open(dict_path, encoding="utf8")as fi:
            words = [line.strip() for line in fi.readlines()]
            self.id2token = dict(enumerate(words))
            self.token2id = dict([(w, i) for i, w in enumerate(words)])

    def encode(self, nodes, is_train=True):
        if is_train:
            nodes = [HEAD] + nodes + [TAIL]
        else:
            nodes = [HEAD] + nodes
        token_ids = [self.token2id.get(n, 1) for n in nodes]
        segment_ids = [0 for _ in nodes]
        return token_ids, segment_ids

    def decode(self, token_ids):
        tokens = [self.id2token.get(i, PAD) for i in token_ids]
        return tokens

    def get_token2id(self):
        return self.token2id

    def get_id2token(self):
        return self.id2token


def padding(token_ids, segment_ids, max_len):
    if len(token_ids) < max_len:
        return token_ids + [0]*(max_len - len(token_ids)), \
               segment_ids + [0]*(max_len - len(token_ids))
    else:
        return token_ids[-max_len:], segment_ids[-max_len:]


tokenizer = Tokenizer(dict_path)  # 建立分词器
# model = build_transformer_model(config_path=config_path,
#                                 checkpoint_path=checkpoint_path,
#                                 model="albert")  # 建立模型，加载权重

# 编码启动
# test_token_ids, test_segment_ids = tokenizer.encode(u"测试")
# test_features = model.predict([np.array([test_token_ids]), np.array([test_segment_ids])])
# print(test_features.shape)
# train, _, _, out_nodes = load_hasA()

node2id = tokenizer.get_token2id()
id2node = tokenizer.get_id2token()
MAX_LENGTH = 64

#
test = ["疾病", "各系统疾病", "胰腺疾病"]
result = tokenizer.encode(test)
result = padding(result[0], result[1], 10)


class NodeDataGenerator(DataGenerator):
    """
    数据生成器
    """
    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids = [], []
        for is_end, item in self.sample(random):
            token_ids, segment_ids = tokenizer.encode(item)
            token_ids, segment_ids = padding(token_ids, segment_ids, MAX_LENGTH)
            batch_token_ids.append(token_ids)
            batch_segment_ids.append(segment_ids)
            if len(batch_token_ids) == self.batch_size or is_end:
                yield [np.array(batch_token_ids), np.array(batch_segment_ids)], None
                batch_token_ids, batch_segment_ids = [], []


class Evaluator(keras.callbacks.Callback):
    def __init__(self, valid_generator, test_generator, my_model):
        self.lowest = float('inf')
        self.valid_generator = valid_generator
        self.test_generator = test_generator
        self.my_model = my_model

    def on_epoch_end(self, epoch, logs=None):
        if logs["loss"] < self.lowest:
            self.lowest = logs["loss"]
            self.my_model.save_weights(model_path)
        print(
            u'current loss: %.5f, lowest loss: %.5f\n' %
            (logs["loss"], self.lowest)
        )


class Loss(Layer):
    """特殊的层，用来定义复杂loss
    """
    def __init__(self, output_axis=None, **kwargs):
        super(Loss, self).__init__(**kwargs)
        self.output_axis = output_axis
        self.supports_masking = True

    def call(self, inputs, mask=None):
        loss = self.compute_loss(inputs, mask)
        self.add_loss(loss)
        if self.output_axis is None:
            return inputs
        elif isinstance(self.output_axis, list):
            return [inputs[i] for i in self.output_axis]
        else:
            return inputs[self.output_axis]

    def compute_loss(self, inputs, mask=None):
        raise NotImplementedError

    def compute_output_shape(self, input_shape):
        if self.output_axis is None:
            return input_shape
        elif isinstance(self.output_axis, list):
            return [input_shape[i] for i in self.output_axis]
        else:
            return input_shape[self.output_axis]

    def get_config(self):
        config = {
            'output_axis': self.output_axis,
        }
        base_config = super(Loss, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class CrossEntropy(Loss):
    """交叉熵作为loss，并mask掉padding部分
    """
    def compute_loss(self, inputs, mask=None):
        y_true, y_pred = inputs
        if mask[1] is None:
            y_mask = 1.0
        else:
            y_mask = K.cast(mask[1], K.floatx())[:, 1:]
        y_true = y_true[:, 1:]  # 目标token_ids
        y_pred = y_pred[:, :-1]  # 预测序列，错开一位
        loss = K.sparse_categorical_crossentropy(y_true, y_pred)
        loss = K.sum(loss * y_mask) / K.sum(y_mask)
        return loss


params = {
    "epochs": 50,
    "batch_size": 64,
}


class NodeGenerationModel:
    def __init__(self, params):
        self.model = None
        self.epochs = params.get("epochs", 5)
        self.batch_size = params.get("batch_size", 64)
        self._build_model()

    def _build_model(self):
        model = build_transformer_model(
            config_path,
            application='lm',
        )
        output = CrossEntropy(1)([model.inputs[0], model.outputs[0]])
        model = Model(model.inputs, output)
        model.compile(optimizer=Adam(1e-5))
        model.summary()
        self.model = model

    def train(self, train, valid, test):
        train_generator = NodeDataGenerator(train, self.batch_size)
        valid_generator = NodeDataGenerator(valid, self.batch_size)
        test_generator = NodeDataGenerator(test, self.batch_size)
        evaluator = Evaluator(valid_generator, test_generator, self.model)
        self.model.fit_generator(train_generator.forfit(),
                                 steps_per_epoch=len(train) // self.batch_size,
                                 epochs=self.epochs,
                                 callbacks=[evaluator],
                                 max_queue_size=10,)
        # self.model.save_weights(model_path)

    def predict(self, input_nodes, expand_length=4, expand_width=2):
        if os.path.exists(model_path):
            self.model.load_weights(model_path)
        curr_pos = len(input_nodes)
        input_nodes = list(input_nodes)
        result = [input_nodes]
        for _ in range(expand_length):
            new_result = []
            for nodes in result:
                if len(nodes) >= MAX_LENGTH - 2:
                    nodes = nodes[len(nodes) - MAX_LENGTH + 2: len(nodes)]
                    # masks = [[1.0]*MAX_LENGTH]
                token_ids, segment_ids = tokenizer.encode(nodes, is_train=False)
                token_ids, segment_ids = padding(token_ids, segment_ids, MAX_LENGTH)
                result = self.model.predict([np.array([token_ids]), np.array([segment_ids])])
                result = result.squeeze().tolist()
                candidates = sorted(enumerate(result[curr_pos]), key=lambda x: x[1], reverse=True)[0:expand_width]
                expanded_nodes = [id2node.get(c[0]) for c in candidates]
                new_result.extend([nodes + [node] for node in expanded_nodes])
            result = new_result[0:]
            curr_pos += 1
        #
        final_result = []
        for r in result:
            new_r = []
            for item in r:
                if item in [PAD, HEAD, UNKNOWN]:
                    continue
                if TAIL == item or item in new_r:
                    break
                new_r.append(item)
            if new_r not in final_result:
                final_result.append(new_r)
        return final_result


# test
if __name__ == "__main__":
    #
    if "isA" == set_type:
        train, valid, test, _ = load_isA()
    elif "hasA" == set_type:
        train, valid, test, _ = load_hasA()
    else:
        train, valid, test = [], [], []
    handler = NodeGenerationModel(params)
    if not os.path.exists(model_path):
        handler.train(train, valid, test)
    # flag = "y"
    # params.update({"flag": "predict"})
    # handler = NodeGenerationModel(params)
    t = ["恐懼症"]
    result = handler.predict(t)
    print("\n".join(map(",".join, result)))
