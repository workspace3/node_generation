# -*- coding: utf-8 -*-
"""
"""
import os
import sys
from bert4keras.backend import keras, K
from bert4keras.models import build_transformer_model
from bert4keras.tokenizers import Tokenizer
import numpy as np
from bert4keras.snippets import DataGenerator
from nltk.translate.bleu_score import corpus_bleu

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


# bert配置
project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../../"
))
config_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/albert_config.json"))
checkpoint_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/model.ckpt-best"
    ))
dict_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/vocab_chinese.txt"
    ))
model_path = os.path.normpath(os.path.join(
    project_dir, "models/best_model.weights"
))

sys.path.insert(0, project_dir)
from src.data_process.load_data import load_hasA


tokenizer = Tokenizer(dict_path, do_lower_case=True)  # 建立分词器
# model = build_transformer_model(config_path=config_path,
#                                 checkpoint_path=checkpoint_path,
#                                 model="albert")  # 建立模型，加载权重

# 编码启动
# test_token_ids, test_segment_ids = tokenizer.encode(u"测试")
# test_features = model.predict([np.array([test_token_ids]), np.array([test_segment_ids])])
# print(test_features.shape)


HEAD = "[CLS]"
TAIL = "[SEP]"
PAD = "[PAD]"
train, _, _, out_nodes = load_hasA()

node2id = {node: i+1 for i, node in enumerate(out_nodes)}
node2id["unknown_node"] = 0
id2node = {node2id[node]: node for node in node2id}
MAX_LENGTH = 12
is_albert_embedding = False


# 获取嵌入矩阵
def embedding(inputs, is_albert=False):
    if is_albert:
        model = build_transformer_model(config_path=config_path,
                                        checkpoint_path=checkpoint_path,
                                        model="albert")  # 建立模型，加载权重
        outputs = []
        for single_input in inputs:
            single_result = []
            single_input = list(single_input)
            for word in single_input:
                token_ids, segment_ids = tokenizer.encode(word)
                features = model.predict([np.array([token_ids]), np.array([segment_ids])])
                sum_features = list(np.sum(features, axis=1))
                sum_features = list(sum_features[0])
                single_result.append(sum_features)
            outputs.append(single_result)
        # 拼接
        outputs = np.array(outputs, dtype=np.float32)
        return outputs
    else:
        outputs = []
        for single_input in inputs:
            single_result = []
            single_input = list(single_input)
            for word in single_input:
                features = [0 for _ in node2id]
                index = node2id.get(word, 0)
                features[index] = 1
                single_result.append(features)
            outputs.append(single_result)
        outputs = np.array(outputs, dtype=np.float32)
        return outputs


embedding_test = [("疾病", "各系统疾病"), ("各系统疾病", "胰腺疾病")]
print(embedding(embedding_test).shape)


class NodeDataGenerator(DataGenerator):
    """
    数据生成器
    """
    def __iter__(self, random=False):
        batch_in_nodes, batch_out_nodes = [], []
        for is_end, item in self.sample(random):
            in_nodes, out_nodes = item
            batch_in_nodes.append(in_nodes)
            batch_out_nodes.append(out_nodes)
            if len(batch_in_nodes) == self.batch_size or is_end:
                max_len = MAX_LENGTH
                batch_in_nodes = [x + [PAD]*(max_len - len(x)) for x in batch_in_nodes]
                batch_out_nodes = [x + [PAD]*(max_len - len(x)) for x in batch_out_nodes]
                batch_out_nodes = [[node2id.get(x, 0) for x in item] for item in batch_out_nodes]
                # masks = [[1.0]*len(x) + [0.0]*(max_len - len(x)) for x in batch_in_nodes]
                yield [embedding(batch_in_nodes, is_albert_embedding),
                       np.array(batch_out_nodes)], None
                batch_in_nodes, batch_out_nodes = [], []


def evaluate(data, my_model):
    total = []
    for (x_true, y_true), _ in data:
        y_pred = my_model.predict([x_true, y_true])[0].argmax(axis=-1)
        y_pred = [[id2node.get(int(x)) for x in item] for item in y_pred]
        y_true = [[[id2node.get(int(x)) for x in item]] for item in y_true]
        total.append(corpus_bleu(y_true, y_pred))
    return np.average(np.array(total))


class Evaluator(keras.callbacks.Callback):
    def __init__(self, valid_generator, test_generator, my_model):
        self.best_val_acc = -1.0
        self.valid_generator = valid_generator
        self.test_generator = test_generator
        self.my_model = my_model

    def on_epoch_end(self, epoch, logs=None):
        val_acc = evaluate(self.valid_generator, self.my_model)
        if val_acc > self.best_val_acc:
            self.best_val_acc = val_acc
            self.my_model.save_weights(model_path)
        test_acc = evaluate(self.test_generator, self.my_model)
        print(
            u'val_acc: %.5f, best_val_acc: %.5f, test_acc: %.5f\n' %
            (val_acc, self.best_val_acc, test_acc)
        )


params = {
    "hidden_size": 200,
    "embedding_size": 768 if is_albert_embedding else len(node2id),
    "layers": 7,
    "epochs": 50,
    "batch_size": 64,
}


class NodeGenerationModel:
    def __init__(self, params):
        self.model = None
        self.epochs = params.get("epochs", 5)
        self.batch_size = params.get("batch_size", 64)
        self._build_model(params)

    def _build_model(self, params):
        hidden_size = params.get("hidden_size", 500)
        embedding_size = params.get("embedding_size", 768)
        output_size = len(node2id)
        layers = params.get("layers", 3)
        inputs = keras.Input(shape=(None, embedding_size))
        inputs_labels = keras.Input(shape=(MAX_LENGTH,), dtype="int64")
        # inputs_masks = keras.Input(shape=(MAX_LENGTH,))
        # if "train" == flag:
        #     hidden_layers = [keras.layers.GRU(units=hidden_size, return_sequences=True, dropout=0.3)
        #                      for _ in range(layers)]
        # else:
        #     hidden_layers = [keras.layers.GRU(units=hidden_size, return_sequences=True)
        #                      for _ in range(layers)]
        hidden_layers = [keras.layers.GRU(units=hidden_size, return_sequences=True)
                         for _ in range(layers)]
        project_layer = keras.layers.Dense(units=output_size, activation="relu")
        output_layer = keras.layers.Dense(units=output_size, activation="softmax")
        hidden_outputs = inputs
        for curr_layer in hidden_layers:
            hidden_outputs = curr_layer(hidden_outputs)
        project_outputs = project_layer(hidden_outputs)
        outputs = output_layer(project_outputs)

        def my_loss(labels, final_outputs):
            """
            自定义损失函数
            """
            # labels = K.expand_dims(labels, -1)
            return K.sparse_categorical_crossentropy(labels, final_outputs)

        loss = keras.layers.Lambda(lambda x: my_loss(*x),
                                   name='my_loss')([inputs_labels, outputs])
        self.model = keras.Model([inputs, inputs_labels], [outputs, loss])
        self.model.add_loss(loss)
        self.model.compile(optimizer="adam", metrics=["accuracy"])
        self.model.summary()

    def train(self, train, valid, test):
        train_generator = NodeDataGenerator(train, self.batch_size)
        valid_generator = NodeDataGenerator(valid, self.batch_size)
        test_generator = NodeDataGenerator(test, self.batch_size)
        evaluator = Evaluator(valid_generator, test_generator, self.model)
        self.model.fit_generator(train_generator.forfit(),
                                 steps_per_epoch=len(train) // self.batch_size,
                                 epochs=self.epochs,
                                 callbacks=[evaluator],
                                 max_queue_size=10,)
        # self.model.save_weights(model_path)

    def predict(self, input_nodes, expand_length=3):
        if os.path.exists(model_path):
            self.model.load_weights(model_path)
        curr_pos = len(input_nodes)
        input_nodes = list(input_nodes)
        for _ in range(expand_length):
            if len(input_nodes) >= MAX_LENGTH:
                inputs = [input_nodes[len(input_nodes) - MAX_LENGTH: len(input_nodes)]]
                # masks = [[1.0]*MAX_LENGTH]
            else:
                inputs = [[HEAD] + input_nodes + [PAD]*(MAX_LENGTH - len(input_nodes) - 1)]
                # masks = [[1.0]*(len(input_nodes) + 1) + [0.0]*(MAX_LENGTH - len(input_nodes) - 1)]
            outputs = [inputs[0][1:] + [PAD]]
            embedded = embedding(inputs, is_albert_embedding)
            outputs_true = np.array([[node2id.get(x, 0) for x in item] for item in outputs])
            # masks = np.array(masks)
            result = self.model.predict([embedded, outputs_true])[0].argmax(axis=-1)
            expanded_node = id2node.get(result[0][curr_pos])
            input_nodes.append(expanded_node)
            curr_pos += 1
        return input_nodes


# test
if __name__ == "__main__":
    #
    train, valid, test, _ = load_hasA()
    handler = NodeGenerationModel(params)
    if not os.path.exists(model_path):
        handler.train(train, valid, test)
    # flag = "y"
    # params.update({"flag": "predict"})
    # handler = NodeGenerationModel(params)
    t = ("胰腺疾病", )
    result = handler.predict(t)
    print(result)
