#! -*- coding: utf-8 -*-
# bert做Seq2Seq任务，采用UNILM方案
# 介绍链接：https://kexue.fm/archives/6933

from __future__ import print_function
import os
import sys
import numpy as np
from bert4keras.backend import keras, K
from bert4keras.layers import Loss
from bert4keras.models import build_transformer_model
from bert4keras.tokenizers import Tokenizer, load_vocab
from bert4keras.optimizers import Adam
from bert4keras.snippets import sequence_padding, open
from bert4keras.snippets import DataGenerator, AutoRegressiveDecoder
from keras.models import Model

# 基本参数
maxlen = 256
batch_size = 32
steps_per_epoch = 360
epochs = 30

# bert配置
project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../../"
))
config_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/albert_config.json"))
checkpoint_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/model.ckpt-best"
    ))
dict_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_base/vocab_chinese.txt"
    ))
model_path = os.path.normpath(os.path.join(
    project_dir, "models/seq2seq_best_model.weights"
))

sys.path.insert(0, project_dir)
from src.data_process.load_data import load_hasA

train, _, _, _ = load_hasA()

# 加载并精简词表，建立分词器
token_dict, keep_tokens = load_vocab(
    dict_path=dict_path,
    simplified=True,
    startswith=['[PAD]', '[UNK]', '[CLS]', '[SEP]'],
)
tokenizer = Tokenizer(token_dict, do_lower_case=True)


class data_generator(DataGenerator):
    """数据生成器
    """
    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids = [], []
        for is_end, single_data in self.sample(random):
            title = "含有".join(single_data[0][1:])
            content = "含有".join(single_data[1][0:-1])
            token_ids, segment_ids = tokenizer.encode(
                content, title, max_length=maxlen
            )
            batch_token_ids.append(token_ids)
            batch_segment_ids.append(segment_ids)
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                yield [batch_token_ids, batch_segment_ids], None
                batch_token_ids, batch_segment_ids = [], []


class CrossEntropy(Loss):
    """交叉熵作为loss，并mask掉输入部分
    """
    def compute_loss(self, inputs, mask=None):
        y_true, y_mask, y_pred = inputs
        y_true = y_true[:, 1:]  # 目标token_ids
        y_mask = y_mask[:, 1:]  # segment_ids，刚好指示了要预测的部分
        y_pred = y_pred[:, :-1]  # 预测序列，错开一位
        loss = K.sparse_categorical_crossentropy(y_true, y_pred)
        loss = K.sum(loss * y_mask) / K.sum(y_mask)
        return loss


model = build_transformer_model(
    config_path,
    checkpoint_path,
    model='albert',
    application='unilm',
    keep_tokens=keep_tokens,  # 只保留keep_tokens中的字，精简原字表
)

output = CrossEntropy(2)(model.inputs + model.outputs)

model = Model(model.inputs, output)
model.compile(optimizer=Adam(1e-5))
model.summary()


class AutoTitle(AutoRegressiveDecoder):
    """seq2seq解码器
    """
    @AutoRegressiveDecoder.set_rtype('probas')
    def predict(self, inputs, output_ids, step):
        token_ids, segment_ids = inputs
        token_ids = np.concatenate([token_ids, output_ids], 1)
        segment_ids = np.concatenate([segment_ids, np.ones_like(output_ids)], 1)
        return model.predict([token_ids, segment_ids])[:, -1]

    def generate(self, text, topk=1):
        max_c_len = maxlen - self.maxlen
        token_ids, segment_ids = tokenizer.encode(text, max_length=max_c_len)
        output_ids = self.beam_search([token_ids, segment_ids],
                                      topk)  # 基于beam search
        return tokenizer.decode(output_ids)


autotitle = AutoTitle(start_id=None, end_id=tokenizer._token_end_id, maxlen=32)


def just_show():
    s1 = u'疾病含有各系统疾病'
    s2 = u'心理疾病含有恐惧症'
    for s in [s1, s2]:
        print(u'生成结果:', autotitle.generate(s))
    print()


class Evaluate(keras.callbacks.Callback):
    def __init__(self):
        self.lowest = 1e10

    def on_epoch_end(self, epoch, logs=None):
        # 保存最优
        if logs['loss'] <= self.lowest:
            self.lowest = logs['loss']
            model.save_weights(model_path)
        # 演示效果
        just_show()


if __name__ == '__main__':
    if not os.path.exists(model_path):
        evaluator = Evaluate()
        train_generator = data_generator(train, batch_size)

        model.fit_generator(
            train_generator.forfit(),
            steps_per_epoch=steps_per_epoch,
            epochs=epochs,
            callbacks=[evaluator]
        )
    else:
        model.load_weights(model_path)
        s = u'疾病含有各系统疾病'
        print(u'生成结果:', ", ".join(autotitle.generate(s).split("含有")))
