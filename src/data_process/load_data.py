# -*- coding: utf-8 -*-
"""
"""
import os
import sys
import pickle
import random


project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../../"
))
sys.path.insert(0, project_dir)

from src.data_process.process import SchemaExtension

data_dir = os.path.normpath(os.path.join(
    project_dir, "data"
))

total_hasA_data_path = os.path.join(data_dir, "hasA_139.txt")
total_isA_data_path = os.path.join(data_dir, "isA_139.txt")

HEAD = "[CLS]"
TAIL = "[SEP]"
PAD = "[PAD]"

out_files_hasA = [os.path.join(data_dir, "hasA_{}.txt".format(key))
                  for key in ["train", "valid", "test", "out_nodes"]]
out_files_isA = [os.path.join(data_dir, "isA_{}.txt".format(key))
                  for key in ["train", "valid", "test", "out_nodes"]]

id2name = SchemaExtension().id2name


def _load(in_f, out_files):
    if all(map(os.path.exists,out_files)):
        out_data = []
        for f in out_files:
            with open(f, "rb")as fi:
                out_data.append(pickle.load(fi))
        return out_data
    else:
        with open(in_f, "rb") as fi:
            data = pickle.load(fi)
            data = list(data)[0:len(data)]
            random.shuffle(data)
            train, valid, test = [], [], []
            nodes_in_train = set()
            for i in range(len(data)):
                data[i] = [id2name.get(x, PAD) for x in list(data[i])]
                mod_i = i % 10
                if 0 <= mod_i <= 6:
                    train.append(data[i])
                    nodes_in_train.update(set(data[i]))
                elif 7 <= mod_i <= 8:
                    test.append(data[i])
                else:
                    valid.append(data[i])
            nodes_in_train = list(nodes_in_train) + [HEAD, PAD, TAIL]
            for out_data, out_file in zip([train, valid, test, nodes_in_train], out_files):
                with open(out_file, "wb")as fo:
                    pickle.dump(out_data, fo)
        return train, valid, test, nodes_in_train


def load_hasA():
    return _load(total_hasA_data_path, out_files_hasA)


def load_isA():
    return _load(total_isA_data_path, out_files_isA)


if __name__ == "__main__":
    result = load_isA()
    print(result[0][1])
    print("训练数据：{}，验证数据：{}，测试数据：{}，所有输出节点数：{}".format(*map(len, result)))
