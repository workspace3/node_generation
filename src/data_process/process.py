# -*- coding: utf-8 -*-
"""
"""
# 加载包
import os
import sys

import pandas as pd
import json
import re
import textdistance
import numpy as np

from datetime import datetime
from collections import defaultdict
from datasketch import MinHash, MinHashLSH

# from opencc import OpenCC
#
# print("{}, 模块加载完毕.".format(datetime.now()))

project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../.."
))
data_dir = os.path.normpath(os.path.join(project_dir, "data/origin"))
category_file = os.path.join(data_dir, "Category.txt")
category_inlinks_file = os.path.join(data_dir, "category_inlinks.txt")
category_outlinks_file = os.path.join(data_dir, "category_outlinks.txt")

# 默认数据路径
DEFAULT_KWARGS = {
    "category_path": category_file,
    "category_inlinks_path": category_inlinks_file,
    "category_outlinks_path": category_outlinks_file,
    "method": "brute-force", # lsh, brute-force, ...
}
# number of permutation for LSH
NUMBER_PERMUTATION = 128


# 通用的函数
def load_text(f):
    """
    读取文本文件
    """
    with open(f, encoding="utf8")as fi:
        data = [line.rstrip("\n") for line in fi.readlines() if len(line.strip()) > 0]
    return data


def write_text(buffer, f):
    """
    写入文本文件
    """
    with open(f, "w", encoding="utf8")as fo:
        fo.write("\n".join(buffer) + "\n")


def similarity_of_name(left, right):
    """
    根据名称计算相似度
    """
    #
    return textdistance.levenshtein.normalized_similarity(left, right)


def rm_dict_dup(d):
    """
    将以list为value的字典中的value去重
    :param d:
    :return:
    """
    for k in d:
        d[k] = sorted(set(d[k]))
    return d


def do_similarity(word, words):
    """
    获取word和words中每个词的相似度
    :param word:
    :param words:
    :return:
    """
    simiarity = [(w, similarity_of_name(word, w)) for w in words]
    return simiarity


class SchemaExtension:
    def __init__(self, kwargs=None):
        if kwargs is None:
            kwargs = DEFAULT_KWARGS
        self._load_dicts(kwargs)
        self.method = kwargs.get("method")
        if "lsh" == self.method:
            self.lsh = MinHashLSH(threshold=0.45, num_perm=NUMBER_PERMUTATION)
            self._create_lsh_model()

    def entity_link(self, query, k=1):
        """
        链接到维基百科里的实体
        :param query: 用户输入
        :param k: 输出前k个实体
        :return:
        """
        if "brute-force" == self.method:
            nodes = self._entity_link_by_brute_force(query, k)
        else:
            nodes = self._entity_link_by_lsh(query, k)
        return nodes

    def find_sub_graph(self, entities, depth=3, width=5):
        """
        输出输入结点附近的子图
        :param entities: 输入的实体集合
        :param depth: 搜索深度
        :param width: 搜索广度
        :return:
        """
        #
        result = []
        # 为了生成不同的id
        num = 1

        def get_next_level_nodes(nodes, relation, num):
            if "hasA" == relation:
                id2links = self.id2outlinks
            elif "isA" == relation:
                id2links = self.id2inlinks
            else:
                id2links = dict()
            #
            tmp_nodes = []
            for head_name in nodes:
                _id = self.name2id.get(head_name)
                curr_nodes = [self.id2name.get(x) for x in id2links.get(_id, []) if x in self.id2name]
                curr_nodes = curr_nodes[0:width]
                tmp_nodes.extend(curr_nodes)
                head_id = "{}".format(num) if head_name not in id_records else id_records.get(head_name)
                id_records[head_name] = head_id
                num += 1
                for tail_name in curr_nodes:
                    tail_id = "{}".format(num) if tail_name not in id_records else id_records.get(tail_name)
                    id_records[tail_name] = tail_id
                    num += 1
                    result.append({"head": head_name, "head_id": head_id, "head_type": "nodes",
                                   "relation": relation,
                                   "tail": tail_name, "tail_id": tail_id, "tail_type": "nodes"})
            return tmp_nodes, num
        #
        down_nodes, up_nodes = entities[0:], entities[0:]
        curr_depth = 0
        id_records = dict()
        while curr_depth < depth:
            curr_depth += 1
            # 获取hasA关系的三元组
            down_nodes, num = get_next_level_nodes(nodes=down_nodes, relation="hasA", num=num)
            # 获取isA关系的三元组
            up_nodes, num = get_next_level_nodes(nodes=up_nodes, relation="isA", num=num)
        return result

    def _load_dicts(self, kwargs):
        """
        加载各种索引表
        :param kwargs: 数据路径的参数字典
        :return:
        """
        id2name = dict()
        name2id = dict()
        word2names = defaultdict(set)
        id2inlinks = defaultdict(list)
        id2outlinks = defaultdict(list)
        category_path = kwargs.get("category_path")
        for line in load_text(category_path):
            parts = line.rstrip().split("\t")
            _, _id, name = parts
            if _id in id2name:
                print("id 有冲突：{}".format(_id), file=sys.stderr)
            else:
                id2name[_id] = name
            if name in name2id:
                print("name 有冲突：{}".format(name), file=sys.stderr)
            name2id[name] = _id
            for word in list(name):
                word2names[word].add(name)
        #
        category_inlinks_path = kwargs.get("category_inlinks_path")
        for line in load_text(category_inlinks_path):
            _id, inlink_id = line.rstrip().split("\t")
            id2inlinks[_id].append(inlink_id)
        #
        category_outlinks_path = kwargs.get("category_outlinks_path")
        for line in load_text(category_outlinks_path):
            _id, outlink_id = line.rstrip().split("\t")
            id2outlinks[_id].append(outlink_id)
        #
        rm_dict_dup(id2inlinks)
        rm_dict_dup(id2outlinks)
        #
        self.id2name, self.name2id, self.id2inlinks, self.id2outlinks, self.word2names = \
            id2name, name2id, id2inlinks, id2outlinks, word2names

    def _create_lsh_model(self):
        """
        构建lsh搜索模型
        :return:
        """
        ss = timeit.default_timer()
        for name in self.name2id:
            words = list(name)
            min_hash = MinHash(num_perm=NUMBER_PERMUTATION)
            for w in words:
                min_hash.update(b=w.encode("utf8"))
            self.lsh.insert(key=name, minhash=min_hash)
        ee = timeit.default_timer()
        print("LSH模型构建耗时: {:.3f} 秒.".format(ee - ss))

    def _entity_link_by_lsh(self, query, k):
        """
        链接到维基百科里的实体, LSH(局部敏感哈希)搜索相似实体
        :param query: 用户输入
        :param k: 输出前k个实体
        :return:
        """
        if query in self.name2id:
            return [query]
        ss = timeit.default_timer()
        min_hash = MinHash(num_perm=NUMBER_PERMUTATION)
        for w in query:
            min_hash.update(b=w.encode("utf8"))
        nodes = self.lsh.query(minhash=min_hash)
        ee = timeit.default_timer()
        print("搜索相似节点耗时：{0:.3f} 秒.".format(ee - ss))
        return nodes[0:k]

    def _entity_link_by_brute_force(self, query, k):
        """
        链接到维基百科里的实体, 暴力搜索 (耗时较长, 可弃用)
        :param query: 用户输入
        :param k: 输出前k个实体
        :return:
        """
        if query in self.name2id:
            return [query]
        #
        filtered_words = set()
        for w in list(query):
            filtered_words.update(self.word2names.get(w, set()))
        filtered_words = list(filtered_words)
        print("{}, 搜索词库的词汇量：{}".format(datetime.now(), len(filtered_words)))
        #
        threshold = 2000
        if len(filtered_words) > threshold:
            is_multi_process = True
        else:
            is_multi_process = False
        if is_multi_process:
            n_part = 4
            length = len(filtered_words)
            step = length // n_part + 1
            inputs = [(t, [k for k in filtered_words[i: i + step]]) for i in range(0, length, step)]
            print("{}, 分为{}个进程...".format(datetime.now(), len(inputs)))
            pool = Pool(n_part)
            similarity_result = []
            ss = timeit.default_timer()
            for curr_word, curr_dict in inputs:
                # print("{}, 子进程添加".format(datetime.now()))
                similarity_result.append(pool.apply_async(do_similarity, (curr_word, curr_dict)))
            pool.close()
            pool.join()
            ee = timeit.default_timer()
            print("搜索相似节点耗时：{0:.3f} 秒.".format(ee - ss))
            similarity = []
            for s in similarity_result:
                similarity.extend(s.get())
        else:
            ss = timeit.default_timer()
            similarity = do_similarity(t, filtered_words)
            ee = timeit.default_timer()
            print("搜索相似节点耗时：{0:.3f} 秒.".format(ee - ss))
        similarity = sorted(similarity, key=lambda x: x[-1], reverse=True)
        top_k = similarity[0:k]
        nodes = [name for name, _ in top_k]
        return nodes


# main
if __name__ == "__main__":
    """
    检索的方式，实现schema的扩展，demo
    """
    from multiprocessing import Pool
    import timeit
    #
    kwargs = {
        "category_path": category_file,
        "category_inlinks_path": category_inlinks_file,
        "category_outlinks_path": category_outlinks_file,
        "method": "lsh",  # lsh, brute-force, ...
    }
    schema_extender = SchemaExtension(kwargs)
    # cc = OpenCC("t2s")
    while True:
        t = input("请输入:")
        start_time = timeit.default_timer()
        #
        entities = schema_extender.entity_link(t)
        result = schema_extender.find_sub_graph(entities)
        end_time = timeit.default_timer()
        print(json.dumps(result, ensure_ascii=False))
        with open("nodes.txt", "w", encoding="utf8")as fo:
            result = {"predict": result}
            fo.write(json.dumps(result, ensure_ascii=False))
        print("处理耗时：{0:.3f} 秒.".format(end_time - start_time))
