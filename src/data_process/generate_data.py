# -*- coding: utf-8 -*-
"""
"""
import os
import sys
import timeit
import time
import pickle
import random
from tqdm import tqdm
from datetime import datetime


project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../../"
))
data_dir = os.path.normpath(os.path.join(
    project_dir, "data"))

sys.path.insert(0, project_dir)
from src.data_process.process import get_dicts


def generate_data():
    """
    生成总的数据
    :return:
    """
    id2name, name2id, id2inlinks, id2outlinks, _ = get_dicts()
    #

    def expand_paths(start_id, direction="isA", is_print=False):
        """
        给定起始点，扩展路径
        :param start_node:
        :return:
        """
        s = timeit.default_timer()
        if is_print:
            print("{}, 开始扩展节点【{}】的{}关系节点".format(datetime.now(), start_id, direction))
            sys.stdout.flush()
        max_depth = 10
        max_width = 10 if "isA" == direction else 20
        min_length = 3
        if "isA" == direction:
            id2links = id2inlinks
        elif "hasA" == direction:
            id2links = id2outlinks
        else:
            id2links = dict()
        out_data = set()
        curr_paths = [(start_id,)]
        count = 0
        while len(curr_paths) > 0:
            curr_path = curr_paths.pop(-1)
            if len(curr_path) >= max_depth:
                out_data.add(curr_path)
                continue
            last_node = curr_path[-1]
            if last_node not in id2links:
                if len(curr_path) >= min_length:
                    out_data.add(curr_path)
                else:
                    pass
                continue
            next_nodes = id2links.get(last_node)
            next_nodes = [node for node in next_nodes if node not in curr_path]
            if len(next_nodes) > max_width:
                next_nodes = random.sample(next_nodes, max_width)
            curr_paths.extend([tuple(list(curr_path) + [_id]) for _id in next_nodes])
            #
            if is_print:
                count += 1
                if count % 100000 == 0:
                    print("{}, 当前缓冲池中路径数:{}, 当前找到的路径数:{}".format(
                        datetime.now(), len(curr_paths), len(out_data)))
                    sys.stdout.flush()
                if count > 10000000:
                    count = 1
            #
        e = timeit.default_timer()
        if is_print:
            print("{}, 扩展节点【{}】的消耗时间: {}".format(datetime.now(), start_id, e - s))
            sys.stdout.flush()
        return out_data
    #
    isA_data = set()
    hasA_data = set()
    count = 0
    # 1745070:各系统疾病
    # 52452:疾病
    target_ids = [
        '52452',
        '260303',
        '279959',
        '316834',
        '316836',
        '316839',
        '316840',
        '316841',
        '317170',
        '317478',
        '317479',
        '317480',
        '317482',
        '322930',
        '425754',
        '525155',
        '531315',
        '605655',
        '1062483',
        '1069305',
        '1075769',
        '1146482',
        '1146495',
        '1280914',
        '1285338',
        '1295468',
        '1409206',
        '1409233',
        '1409587',
        '1463418',
        '1463902',
        '1523843',
        '1526779',
        '1544270',
        '1558277',
        '1558279',
        '1559815',
        '1559817',
        '1614356',
        '1614369',
        '1637595',
        '1637613',
        '1654763',
        '1703673',
        '1707223',
        '1707225',
        '1707234',
        '1708995',
        '1709005',
        '1711005',
        '1745070',
        '1745609',
        '1745633',
        '1749748',
        '1768429',
        '1774161',
        '1778938',
        '1778939',
        '1779870',
        '1779871',
        '1780835',
        '1780898',
        '1805951',
        '1818250',
        '1822276',
        '1822288',
        '1830462',
        '1830464',
        '1830536',
        '1830542',
        '1830575',
        '1830578',
        '1830582',
        '1830610',
        '1830680',
        '1830682',
        '1830688',
        '1830704',
        '2956802',
        '2956811',
        '3203705',
        '3225345',
        '3360342',
        '3497844',
        '3803321',
        '3836635',
        '3840565',
        '3841777',
        '3900365',
        '3900369',
        '3915199',
        '4030363',
        '4063782',
        '4076847',
        '4344367',
        '4381675',
        '4381677',
        '4381678',
        '4432871',
        '4432873',
        '4433316',
        '4433375',
        '4438075',
        '4985149',
        '4985151',
        '4985157',
        '5362247',
        '5368411',
        '5368413',
        '5419921',
        '5420251',
        '5420253',
        '5420255',
        '5422548',
        '5486916',
        '5528421',
        '5569431',
        '5667784',
        '5919214',
        '5939891',
        '5939895',
        '6023135',
        '6027469',
        '6042940',
        '6106041',
        '6151967',
        '6227813',
        '6243479',
        '6254976',
        '6281667',
        '6281668',
        '6297676',
        '6330513',
        '6330514',
        '6385522',
        '6748984',
        '6764662',
        '6800956',
        '6839097',
    ]
    for start_id in tqdm(target_ids):
        count += 1
        isA_data.update(expand_paths(start_id, "isA"))
        hasA_data.update(expand_paths(start_id, "hasA"))
        if count % 5000 == 0 or count == len(target_ids):
            out_isA_file = os.path.join(data_dir, "isA_{}.txt".format(count))
            with open(out_isA_file, "wb")as fo_isA:
                pickle.dump(isA_data, fo_isA)
            out_hasA_file = os.path.join(data_dir, "hasA_{}.txt".format(count))
            with open(out_hasA_file, "wb")as fo_hasA:
                pickle.dump(hasA_data, fo_hasA)
            isA_data = set()
            hasA_data = set()
    print("{}, done.".format(datetime.now()))


if __name__ == "__main__":
    generate_data()
